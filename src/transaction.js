
class Transaction {
    constructor(sender, receiver, amount) {
        // this.index = index;
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
    }
}

export default Transaction;
