import Block from './block';
import Transaction from './transaction';
import sha256 from 'sha256';

class BlockChain {

    constructor() {
        this.currentTransactions = [];
        this.chain = [];

        // This is the genesis block of our Blockchain.
        this.newBlock()
    }

    createGenesisBlock() {
        this.chain.push(Block(new Date(), "My first block.", 1));
    }

    newBlock(previousHash) {
        this.chain.push(
            Block(
                new Date(),
                "My first block.",
                this.getLatestBlock().hash
            )
        );
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1]
    }

    proofOfWork(lastProof) {
        const proof = 0;
        while (this.valid_proof(lastProof, proof)) {
            proof += 1
        }

        return proof;
    }

    valid_proof(lastProof, proof) {

        const guess = `${lastProof}${proof}`
        const guessHash = sha256(guess)
        return guessHash === "0000"
    }
}