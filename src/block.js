import sha256 from 'sha256';
import Transaction from './transaction';

class Block {
    constructor(timestamp, transactions, prevHash) {
        this.index = index;
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.prevHash = prevHash;
        this.hash = sha256(
            this.timestamp + this.data + this.prevHash
        );
    }
}

export default Block;